hashcash (1.22-2) UNRELEASED; urgency=medium

  * d/control:
    - Bump Standards-Version to 4.6.2, no changes needed.
  * d/gbp.conf: New file.
  * d/upstream/metadata: New file.

 -- Stefan Kangas <stefankangas@gmail.com>  Fri, 27 Jan 2023 10:48:23 +0200

hashcash (1.22-1) unstable; urgency=medium

  * New maintainer. (Closes: #831803)
    Thanks to Hubert Chan for previous work.
  * New upstream release. (Closes: #962820)
  * Actually enable hardened build flags. (Closes: #655864)
  * debian/control:
    - Set debhelper-compat version in Build-Depends.
    - Bump Standards-Version to 4.6.1.
    - Set Rules-Requires-Root: no.
  * debian/rules: Update to modern debhelper. (Closes: #999030, #928962)
  * debian/watch:
    - Update watch file format version to 4.
    - Opportunistically check upstream PGP signatures.
  * Use 3.0 (quilt) source format.
  * debian/copyright: Use standard, machine-readable format.
  * Fix typos in hashcash(1) man page.
  * Don't use bundled getopt.
  * Add doc-base entry.

 -- Stefan Kangas <stefankangas@gmail.com>  Tue, 11 Oct 2022 16:21:51 +0200

hashcash (1.21-2) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer (See: #831803)
  * Update DH level to 10. (Closes: #817490)
  * debian/compat: updated to 10.
  * debian/control:
     - Bumped Standards-Version to 3.9.8.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Thu, 15 Dec 2016 10:48:59 -0200

hashcash (1.21-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Enable hardened build flags (Closes: #655864)

 -- Moritz Muehlenhoff <jmm@debian.org>  Sun, 08 Apr 2012 12:24:59 +0200

hashcash (1.21-1) unstable; urgency=low

  * New upstream release.
  * Fix copyright file.

 -- Hubert Chan <hubert@uhoreg.ca>  Thu, 30 Mar 2006 21:01:13 -0700

hashcash (1.20-1) unstable; urgency=low

  * New upstream release.
  * Set DEB_HOST_ARCH if it is not set, so that debian/rules works properly
    even if not run form dpkg-buildpackage.
  * Bump standards version to 3.6.2.

 -- Hubert Chan <hubert@uhoreg.ca>  Thu, 22 Dec 2005 00:00:43 -0500

hashcash (1.17-1) unstable; urgency=low

  * New upstream release.
    * Adds hashfork.py script to run multiple instances of Hashcash on
      multiprocessor machines.  (closes: 281918)

 -- Hubert Chan <hubert@uhoreg.ca>  Wed, 30 Mar 2005 15:07:08 -0500

hashcash (1.13-1) unstable; urgency=low

  * New upstream release.
  * Fix compile flags for amd64 architecture so that it compiles.
    (closes: 278047)

 -- Hubert Chan <hubert@uhoreg.ca>  Thu,  4 Nov 2004 17:41:28 -0500

hashcash (1.12-1) unstable; urgency=low

  * New upstream release. (closes: 271635)
  * Add architecture-dependent flags in debian/rules to enable assembly
    optimizations.
  * New license from upstream.

 -- Hubert Chan <hubert@uhoreg.ca>  Fri,  8 Oct 2004 23:17:32 -0400

hashcash (1.01-1) unstable; urgency=low

  * New upstream release.
  * Clarified license as per upstream.
  * Compile with -Wall.

 -- Hubert Chan <hubert@uhoreg.ca>  Sun,  8 Aug 2004 19:31:43 -0400

hashcash (1.00-1) unstable; urgency=low

  * New upstream release.
  * This release adds the new stamp format.  (closes: 239157)
  * Update of Kyle Hasselbacher's scripts.
  * Add link to the hashcash-sendmail page in copyright file and in
    hashcash-sendmail.txt.
  * New license from upstream.
  * Add rule to debian/rules to rebuild documentation if needed.
  * Apply patch from Justin to fix segfault when checking future version
    stamps.

 -- Hubert Chan <hubert@uhoreg.ca>  Sun,  8 Aug 2004 00:09:33 -0400

hashcash (0.32-1) unstable; urgency=low

  * New upstream release.
  * Updated copyright file to add new request by author.
  * Change upstream website in copyright file to reflect new address.
  * Add Kyle Hasselbacher's hashcash-sendmail and hashmail-request scripts
    (ALPHA) to examples directory.

 -- Hubert Chan <hubert@uhoreg.ca>  Wed, 26 May 2004 23:47:13 -0400

hashcash (0.28-4) unstable; urgency=low

  * Remove beginning "a" from package description.
  * Fixed typos in changelog, and improved consistency.
  * Re-add fip180-1.txt to source package.  (Not needed for binary
    package.)  All FIPS are public domain worldwide, as per discussion on
    debian-legal.
    http://lists.debian.org/debian-legal/2004/debian-legal-200403/msg00028.html

 -- Hubert Chan <hubert@uhoreg.ca>  Thu,  4 Mar 2004 15:42:31 -0500

hashcash (0.28-3) unstable; urgency=low

  * Don't install sha1 or sha1.1.  Debian already has sha1sum.
  * Remove fip180-1.txt -- copyright status uncertain.
  * Fix the copyright file to make it clear that Debian does not
    specifically make the same pledges that upstream does.

 -- Hubert Chan <hubert@uhoreg.ca>  Fri, 27 Feb 2004 21:16:19 -0500

hashcash (0.28-2) unstable; urgency=low

  * Bump policy version to 3.6.1.
  * Add note to copyright file about getopt being GPL licensed.

 -- Hubert Chan <hubert@uhoreg.ca>  Sun, 22 Feb 2004 21:32:22 -0500

hashcash (0.28-1) unstable; urgency=low

  * Initial Release. (closes: 234743)

 -- Hubert Chan <hubert@uhoreg.ca>  Sat, 27 Dec 2003 15:23:30 -0500
